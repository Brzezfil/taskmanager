package com.projectgroup.ManagerTask.front;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomepageController {

    @GetMapping("/")
    String showIndexPage(){
        return "index";
    }
}
