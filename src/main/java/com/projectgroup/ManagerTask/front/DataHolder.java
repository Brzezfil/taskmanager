package com.projectgroup.ManagerTask.front;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataHolder {

    private String name;
    private String value;
    private int quantity;

}
